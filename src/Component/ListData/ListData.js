import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {getProductCategory} from '../../api';
import {SearchInput} from '../SearchInput/Input';

export const ListData = () => {
  const [productList, setProductList] = useState(null);
  const [search, setSearch] = useState('');

  const searchProduct = text => {
    setSearch(text);
  };

  const getProductList = async () => {
    try {
      const response = await getProductCategory();
      setProductList(
        response.data.products?.filter(searchData => {
          return searchData?.title
            ?.toLowerCase()
            .includes(search?.toLowerCase());
        }),
      );
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getProductList();
  }, [search]);

  const ProductItem = ({dataItem}) => (
    <View style={styles.items}>
      <View style={styles.leftContent}>
        <Text style={styles.title}>{dataItem.title}</Text>
        <Text style={styles.description}>{dataItem.description}</Text>
      </View>
      <View style={styles.rightContent}>
        <Text style={styles.price}>${dataItem.price}</Text>
      </View>
    </View>
  );

  return (
    <View style={styles.listContainer}>
      <Text style={styles.headerTitle}>Product List</Text>
      <View style={styles.searchContainer}>
        <SearchInput
          placeholder="Search Products"
          onChangeText={text => searchProduct(text)}
        />
      </View>
      <FlatList
        data={productList}
        renderItem={({item}) => <ProductItem dataItem={item} />}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
    margin: 10,
  },
  headerTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#1E4EC3',
  },
  searchContainer: {
    marginBottom: 15,
  },
  items: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 10,
    margin: 10,
    padding: 10,
  },
  leftContent: {
    width: 280,
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'black',
  },
  description: {
    fontSize: 16,
    color: 'black',
    marginTop: 10,
  },
  rightContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  price: {
    fontSize: 18,
    color: '#1E4EC3',
    textAlignVertical: 'center',
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
});
