import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

export const SearchInput = ({placeholder,onChangeText}) => {
  return (
    <TextInput
      style={styles.input}
      placeholder={placeholder}
      onChangeText={onChangeText}
      placeholderTextColor="black"
    />
  );
};

const styles = StyleSheet.create({
  input: {
    padding: 10,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    fontSize: 18,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#48BBEC',
    backgroundColor: '#E1EDEF',
    color: "black"
  },
});
