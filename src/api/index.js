
import axios from "axios";
import { endPoints } from "./endPoint";

export const getProductCategory = async () => {
    let request = await axios({
      method: "get",
      url: endPoints.products.GET_PRODUCTS,
    });
    return request;
};