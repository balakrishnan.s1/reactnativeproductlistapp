export const BASE_URL = 'https://dummyjson.com';

export const endPoints = {
  products: {
    GET_PRODUCTS: `${BASE_URL}/products`,
  },
};
